# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you


def calculate_average(values):
    if type(values) == list:
        for num in values:
            if type(num) == int or type(num) == float:
                continue
            else:
                return print("Values are not numerical")
        return print(sum(values)/len(values))
    else:
        return print("Input must be a list")

y = calculate_average([1,2,3,4])
print(y)

# def calculate_average(values):
#     if len(values) == 0:
#         return None
#     return sum(values)/len(values)

# x = calculate_average([1,3,3,4])
# print(x)

