# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    x = "Valid Password"
    y = "Invalid Password"
    if len(password) >= 6 and len(password) <= 12:
        for char in password:
            if char.islower() == True:
                continue
            if char.isupper() == True:
                continue
            if char.isdigit() == True:
                continue
            if char == "@" or char == "!" or char == "$":
                return x
            else:
                return y
    else:
        return y


x = check_password("hackreactor!1")
print(x)