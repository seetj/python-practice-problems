# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


def halve_the_list(list):
    newlist1 = []
    newlist2 = []
    breakpoint = round(len(list) / 2)
    for value in list:
        newlist1.append(value)
        if len(newlist1) == breakpoint:
            break
    for value in list:
        if value not in newlist1:
            newlist2.append(value)

    return(newlist1,newlist2)

x = halve_the_list([1,2,3,4,5,6,7])
print(x)
        
       

